// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

/*
	===== TO CREATE A BASIC BRIEFING =====
	The following code will add a "basic" briefing to all units in the mission

	player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
	player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
	player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];


	===== TO CREATE A SIDE-SPECIFIC BRIEFING =====
	The following code will add a briefing *only* to a certain side
	In this example, a briefing will be created that is only visible to BLUFOR players
	UNLESS YOUR MISSION HAS MULTIPLE PLAYER SIDES. YOU DO NOT NEED THIS CODE.
	
	if ((side player) == west) then {
		player createDiaryRecord ["Diary", ["Mission", "BLUFOR mission notes go here"]];
	};
	
	
	===== TO CREATE A ZEUS-SPECIFIC BRIEFING =====
	The following code will add a briefing *only* to player zeus units.
	
	if (player isKindOf "VirtualCurator_F")then {
		player createDiaryRecord ["Diary", ["Zeus Notes", "Zeus notes go here"]];
	};
	
	
	===== NOTES =====
	Keep in mind that even with these if-statements, briefings will still appear in *reverse order from which they are written*
	This means if you want an extra note for a specific side that goes at the bottom of the briefing, that briefing should go at the top of this file.
*/

player createDiaryRecord ["Diary", ["Assets", "Headquarters has provided you with the following equipment for this mission:<br/>
	- 5x HMMWV Transport<br/>
	- 1x Multiple-Rocket-Launch system
"]];

player createDiaryRecord ["Diary", ["Intel",
"The RF radiation from the enemy artillery is affected substantially by terrain, so you'll likely need to get to an elevated position to get an accurate reading.
<br/><br/>Do not under any circumstances cross the red-marked ""Rebel Line"". There is heavy enemy presence on the north-eastern side of the line, crossing it will mean almost certain death.
<br/><br/>Do not bring vehicles across the blue-marked ""Government Line"". Rebel observers will easily be able to identify your position from the vehicle sounds, and will not hesistate to send a QRF your way.
<br/><br/>This mission is a one-life op. You will not respawn if you are killed. Play it safe, you can't report your findings if you're dead.
<br/><br/>Do not fire the MRL until you have identified the locations of the five rocket artillery pieces, and all friendly forces are back at SIGINT Field HQ. Rockets aren't exactly the stealthiest tool, and you can expect a strong response in-kind shortly after you start firing."
]];

player createDiaryRecord ["Diary", ["Mission",
"Your mission today is to locate and destroy five enemy rocket artillery vehicles before they can launch their next salvo. The barrages usually start around 6:45am, so you'll only have an hour and a half tops to finish the job.
<br/><br/>The enemy rocket artillery pieces are networked together to an advanced fire control system, which allows them to accurately coordinate strikes from multiple launch locations. However, this network functionality means that the rocket artillery is constantly emitting RF radiation, which your spectrum devices should be able to pick up on.
<br/><br/>Your EW specialists should use their spectrum devices to triangulate the positions of the rocket artillery pieces, and the MRL you have been provided with should be used to deliver the killing blow."
]];

player createDiaryRecord ["Diary", ["Situation",
"The civil war in Ihantala rages on, with rebel forces maintaining control of the north-eastern quadrant. Government force defensive lines are being shelled repeatedly by rocket artillery, with no end in sight. Despite their best efforts, the main government force has so far been unable to locate the rocket artillery, deep behind enemy lines.
<br/><br/>Early one morning, a small team of highly trained special forces operatives from the signals intelligence division were deployed to the region. Government command believes that this may be the key to elimninating the persistent rocket artillery threat."
]];
