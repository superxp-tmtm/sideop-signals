// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	class example
	{
		displayName = "Example Loadout"; // Currently unused, basically just a human-readable name for the loadout
		
		// Weapon definitions all use the following format:
		// {Weapon Classname, Suppressor, Pointer (Laser/Flashlight), Optic, [Primary magazine, ammo count], [Secondary Magazine (GL), ammo count], Bipod}
		// Any empty definitions must be defined as an empty string, or an empty array. Otherwise the loadout will not apply correctly.
		
		primaryWeapon[] = {"arifle_MXC_F", "", "acc_flashlight", "optic_ACO", {"30Rnd_65x39_caseless_mag",30}, {}, ""}; // Primary weapon definition
		secondaryWeapon[] = {"launch_B_Titan_short_F", "", "", "", {"Titan_AP", 1}, {}, ""}; // Secondary weapon (Launcher) definition.
		handgunWeapon[] = {"hgun_ACPC2_F", "", "", "", {"9Rnd_45ACP_Mag", 9}, {}, ""}; // Handgun definition
		binocular = "Binocular";
		
		uniformClass = "U_B_CombatUniform_mcam_tshirt";
		headgearClass = "H_Watchcap_blk";
		facewearClass = "";
		vestClass = "V_Chestrig_khk";
		backpackClass = "B_AssaultPack_mcamo";
		
		// Linked items requires all six definitions to be present. Use empty strings if you do not want to add that item.
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", ""}; // Linked items for the unit, must follow the order of: Map, GPS, Radio, Compass, Watch, NVGs.
		
		// When placed in an item array, magazines should also have their ammo count defined
		uniformItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
	};
	
	class example_random
	{
		displayName = "Random Loadouts";
		class random_1
		{
			// Loadout info goes here
		};
		class random_2
		{
			// Loadout info goes here
		};
	};
	
	// Empty loadout with comments removed. Use this for your loadouts
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"", "", "", "", {}, {}, ""};
		binocular = "";
		
		uniformClass = "";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", "NVGoggles"};
		
		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class B_Soldier_F: base {
		// Requires the following DLC:
		// Apex
		displayName = "Rifleman";

		primaryWeapon[] = {"CUP_arifle_AK19_bicolor","","CUP_acc_Flashlight","CUP_optic_MicroT1_coyote",{"CUP_30Rnd_556x45_TE1_Tracer_Green_AK19_Tan_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"CUP_hgun_M17_Black","","acc_flashlight_pistol","",{"CUP_21Rnd_9x19_M17_Black",21},{},""};

		uniformClass = "tmtm_u_combatUniformC_sage";
		headgearClass = "CUP_H_RUS_6B47_headset";
		facewearClass = "CUP_G_ESS_KHK_Scarf_Face_Tan";
		vestClass = "CUP_Vest_RUS_6B45_Sh117_Green";

		linkedItems[] = {"ItemMap","","TFAR_anprc152","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ACE_EntrenchingTool",1},{"CUP_17Rnd_9x19_M17_Black",2,17},{"SmokeShell",3,1}};
		vestItems[] = {{"Chemlight_green",2,1},{"HandGrenade",2,1},{"CUP_30Rnd_556x45_TE1_Tracer_Green_AK19_Tan_M",7,30},{"MiniGrenade",2,1},{"SmokeShell",2,1},{"ACE_HandFlare_Red",3,1},{"ACE_HandFlare_Green",3,1},{"ACE_HandFlare_Yellow",3,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_morphine",2},{"ACE_epinephrine",2}};
		basicMedVest[] = {};
	};

	class B_Soldier_SL_F: B_Soldier_F {
		displayName = "Squad Leader";

		facewearClass = "CUP_G_ESS_BLK_Scarf_Face_Red";
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";
	};
	
	class B_Officer_F: B_Soldier_SL_F {
		displayName = "Commander";

		facewearClass = "CUP_G_Grn_Scarf_Shades_Beard";
		headgearClass = "H_Beret_EAF_01_F";
	};
	
	class B_Medic_F: B_Soldier_F {
		displayName = "Medic";

		facewearClass = "CUP_G_ESS_BLK_Scarf_Face_White";
		backpackClass = "B_AssaultPack_sgg";
		
		basicMedBackpack[] = {{"ACE_fieldDressing",50},{"ACE_bloodIV",5},{"ACE_bloodIV_500",5},{"ACE_morphine",10},{"ACE_epinephrine",20}};
	};
	
	class B_W_RadioOperator_F: B_Soldier_F {
		displayName = "EW Specialist";

		handgunWeapon[] = {"hgun_esd_01_F","muzzle_antenna_01_f","acc_esd_01_flashlight","",{},{},""};

		facewearClass = "CUP_G_ESS_BLK_Scarf_Face_Grn";
		
		uniformItems[] = {{"ACE_EntrenchingTool",1},{"SmokeShell",3,1}};
	};
};